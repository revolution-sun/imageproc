#include <AFMotor.h>
#include <Servo.h>
#define MOTOR_ENABLE 1
#define SERVO_ENABLE 0
AF_DCMotor motor_left(1,MOTOR12_64KHZ);
AF_DCMotor motor_right(3,MOTOR12_64KHZ);
int i = 0;
Servo servo_yaw;
Servo servo_pitch;
int servo_yaw_pos = 0;
int pos = 0;
int servo_yaw_flag = 1;
void setup() {
  // put your setup code here, to run once:
  /*
   * communication
   */
  Serial.begin(9600);
  //
  motor_left.setSpeed(200);
  motor_right.setSpeed(200);
  servo_yaw.attach(10);   //this servo is SER1
  servo_pitch.attach(9);  //this serco is SERVO_2
  servo_yaw_pos = servo_yaw.read();
}

String str = "";
void loop() 
{
  // put your main code here, to run repeatedly:
  i += 10;
  int inByte;
  if(MOTOR_ENABLE)
  {
    while(Serial.available() > 0)
    {
      inByte = int(Serial.read());
      if(inByte != 13)
      {
        str += inByte - '0';
        delay(2);
        
      }
        
    }
    
    motor_left.run(FORWARD);    //FORWARD: backward
    delay(100);
    //Serial.print("Speed:");
    delay(100);
    if(str.length() > 0)
    {
      //motor_left.setSpeed(int(str));
      Serial.println(str);
      
    }
    //motor_right.run(BRAKE);   //FORWARD: forward
    str = "";
  }
  if(SERVO_ENABLE)
  {

      servo_yaw_pos = servo_yaw.read();

    for(pos = 0; pos<=180; pos+=30)
    {
        servo_yaw.write(pos);
        servo_yaw_pos = servo_yaw.read();
        delay(1000);
        Serial.println(servo_yaw_pos);


    }
  }
  //Serial.println(servo_yaw_pos);
  delay(1000);
}
