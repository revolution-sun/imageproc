# library.py

'''
class Base:
    def foo(self):
        return 'foo'

    def foo1(self):
        return self.bar()
old_bc = __build_class__
def my_bc(fun, name, base=None, **kw):
    # print('my buildclass-->', a, kw)
    if base is Base:
        print('check if bar method defined')
    if base is not None:
        print('none base')
        return old_bc(fun, name, base, **kw)
    return old_bc(fun, name, **kw)

import builtins

builtins.__build_class__ = my_bc
'''


class BaseMeta(type):
    def __new__(cls, name, bases, body):
        if 'bar' not in body:
            print(body)
            raise TypeError('Bad user class')
        return super().__new__(cls, name, bases, body)


class Base(metaclass=BaseMeta):
    def foo(self):
        return self.bar()
