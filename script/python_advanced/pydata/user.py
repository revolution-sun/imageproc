# user.py
from library import Base
from dis import dis
# if the library has change the function name?
assert hasattr(Base, 'foo'), 'you broke it, you fool!'


class Derived(Base):
    def bar(self):
        return self.foo()

    def fun(self):
        pass

d1 = Derived()

# print(d1.bar())
# dis(Derived)
