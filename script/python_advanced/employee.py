# python object-oirented programming
import datetime
import requests

class Employee:

    raise_amount = 1.2
    num_of_emps = 0

    def __init__(self, first, last, pay):
        self.first = first
        self.last = last
        self.pay = pay
        self.email = first + '.' + last + '@email.com'

        Employee.num_of_emps += 1

    def fullname(self):
        return '{} {}'.format(self.first, self.last)

    def apply_raise(self):
        self.pay = int(self.pay * self.raise_amount)

    # classmethod
    @classmethod
    def set_raise_amt(cls, amount):
        cls.raise_amt = amount

    @classmethod
    def from_sring(cls, emp_str):
        first, last, pay = emp_str.split('-')
        return cls(first, last, pay)

    @staticmethod
    def is_workday(day):
        if day.weekday() == 5:
            return False
        return True

    def __repr__(self):
        return "Employee('{}', '{}', '{}')".format(self.first, self.last,
                        self.pay)

    def __str__(self):
        return '{}-{}'.format(self.fullname(), self.email)

    def __add__(self, other):
        return self.pay + other.pay

    def __len__(self):
        return len(self.fullname())


class Employee1:

    raise_amount = 1.2
    num_of_emps = 0

    def __init__(self, first, last, pay):
        self.first = first
        self.last = last
        self.pay = pay

    @property
    def email(self):
        return '{}.{}@email.com'.format(self.first, self.last)

        Employee.num_of_emps += 1

    @property
    def fullname(self):
        return '{} {}'.format(self.first, self.last)

    @fullname.setter
    def fullname(self, name):
        first, last = name.split(' ')
        self.first = first
        self.last = last

    @fullname.deleter
    def fullname(self):
        print('delet name!')
        self.first = None
        self.last = None

    def apply_raise(self):
        self.pay = int(self.pay * self.raise_amount)

    def monthly_schedule(self, month):
        response = requests.get(f'http://company.com/{self.last}/{month}')
        if response.ok:
            return response.text
        else:
            return 'Bad Response'


class Developer(Employee):
    raise_amt = 1.10

    def __init__(self, first, last, pay, pro_lang):
        super().__init__(first, last, pay)
        # Employee.__init__(self, first, last, pay)
        self.pro_lang = pro_lang


class Manager(Employee):

    def __init__(self, first, last, pay, employees=None):
        super().__init__(first, last, pay)

        if employees is None:
            self.employees = []
        else:
            self.employees = employees

    def add_emp(self, emp):
        if emp not in self.employees:
            self.employees.append(emp)

    def remove_emp(self, emp):
        if emp in self.employees:
            self.employees.remove(emp)

    def print_emp(self):
        for emp in self.employees:
            print('--->', emp.fullname())



emp1_1 = Employee1('John', 'Smith', 9090)

emp1_1.first = 'Jim'
print(emp1_1.first)
print(emp1_1.email)


emp1_1.fullname = 'Cor Scharr'
print('fullname')
print(emp1_1.fullname)
del emp1_1.fullname
print('now')
print(emp1_1.fullname)

emp_1 = Employee('Cover', 'Schafer', 5000)
emp_2 = Employee('test', 'User', 8000)
# Employee.raise_amount is a global variable by all instances
# emp_1.raise_amount is a local variable used only by emp_1

# print(emp_1)
# print(emp_2)
# print(Employee.fullname(emp_1)
print(emp_1.pay)
emp_1.apply_raise()
print(emp_1.pay)

print(Employee.raise_amount)
emp_1.raise_amount = 1.2
print(emp_1.raise_amount)
print(Employee.raise_amount)
print(emp_1.num_of_emps)

# Employee.set_raise_amt(1.4)
# print(Employee.raise_amt)


emp_str_1 = 'John-Deo-5000'
first, last, pay = emp_str_1.split('-')
# new_emp_1 = Employee(first, last, pay)

# new_emp_2 = Employee.from_sring(emp_str_1)
# print(new_emp_1.email)

#my_date = datetime.date(2016, 7, 11)

#print(Employee.is_workday(my_date))
#print(emp_1.is_workday(my_date))


#dev_1 = Employee('Cover', 'Schafer', 5000)
dev_2 = Employee('test', 'User', 8000)

# print(help(Developer))

mgr_1 = Manager('Sue', 'Smith', 9000)

# print(mgr_1.email)
# print(isinstance(mgr_1, Manager))
# print(issubclass(Developer, Employee))
# print(emp_1.__str__())
# print(repr(emp_1))
# print(str(emp_2))

# print(emp_1 + emp_2)
# print(len(emp_1))
