import numpy as np


# not
for i in [0, 1, 2, 3, 4]:
    print(i**2)

# good

for i in range(5):
    print(i ** 2)
# better
for i in xrange(5):
    print(i ** 2)

# not
colors = ['r', 'g', 'b', 'y']
for i in range(len(colors)):
    print(colors[i])
# better
for color in colors:
    print(color)
# #not
for i in range(len(colors), -1, -1):
    print(colors[i])

# better
for color in reversed(colors):
    print(color)

# not
for i in range(len(colors)):
    print i, '--->' colors[i]

# better
for i, color in enumerate(colors):
    print()

# not
names = ['raymond', 'rachel', 'matthew']
num = min(len(names,colors))
for i in range(num):
    print name[i],'-->', colors[i]
# better
for name, color in zip(names, colors):
    print()
# izip --> min?
for name, color in izip(names, colors):
    print()

# color
for color in sorted(colors):
    print
# reverted
for color in sorted(colors, reverse=True):
    print

# not
def compare_length(c1, c2):
    if len(c1) < len(c2): return -1
    if len(c1) > len(c2):return 1
    return 0
print(sorted(colors, cmp=compare_length))

# better
print(sorted(colors, key=len))

# call a function until a sentinel value
# not
blocks = []
while True:
    block = f.read(32)
    if block == ' ':
        break
    blocks.append(block)
# better
blocks = []
for block in iter(partial(f.read, 32), ''):
    blocks.append(block)

# # not
def find(seq, tgt):
    found = False
    for i, value in enumerate(seq):
        if value == tgt:
            found = True
            break
    if not found:
        return -1
    return 1
# better

def find(seq, tgt):
    for i, Value in enumerate(seq):
        if Value == tgt:
            break
    else:
        return -1
    return 1

# master dictionaries is a foundamental python skill
# they are foundamental tool for expressing relationships, linking, counting, and group
# not
d = {'mat':'b', 'rachel':'g', 'ray':'r'}

for key in d:
    print k
# better
for key in d.keys:
    if k.startswitch('r'):
        del d[k]
# not
for k in d:
    print(k, d[k])

# better
for k, v in d.items():
    print(k, v)
# even better
for k, v in d.iteritems():
    print

#
d = dict(izip(names, colors))

# counting with dictionaries

# not
d = {}

for color in colors:
    if color not in d:
        d[color] = 0
    d[color] += 1
# better

for color in colors:
    d[color] = d.get(color, 0) + 1
# grouping with dictinaties
# not
names = ['rat', 'bey', 'john', 'peter', 'roger', 'matthew']

d = {}

for name in names:
    key = len(name)
    if key not in d:
        d[key] = []
    d[key].append(name)

# better

d = {}
for name in names:
    key = len(names)
    d.setdefault(key, []).append(name)

# even more better ?
d = defaultdict(list)
for name in names:
    key = len(name)
    d[key].append(name)

# Not
while d:
    key, value = d.popitem()
    print
# better


# linking dictionaries
defaults = {'color': 'r', 'user': 'guest'}
parser = argparse.ArgumentParser()
parser.add_argument('-u', '--user')
parser.add_argument('-c', '--color')
namespace = parser.parse_args([])
cmd_line_args = {k: v for k, v in vars(namespace).items() if v}

#
d = default.copy()
d.update(os.environ)
d.update(cmd_line_args)

d = ChainMap(cmd_line_args, os.environ, defaults)

# # NOT
twitter_search('@obama', False, 20, True)

# better
twitter_search('@obam', test=False, num=20, done=True)

#
p = 'Reamy', 'Hetti', 0x30, 'email'

## NOT
fname = p[0]
lname = p[1]
age = p[2]
email = p[3]

# better
fname, lname, age, email = p

# update multiple state variable

# not

def fibonacci(n):
    x = 0
    y = 1
    for i in range(n):
        print(x)
        t = y
        y = x + y
        x = t

# better

def fibonacci(n):
    x, y = 0, 1
    for i in range(n):
        print(x)
        x, y = y, x+y

# not

tm_x = x + dx * t
tm_y = y + dy * t
tm_dx = func(m, x, y, dx, dy)
tm_dy = func(m, x, y, dx, dy)
x = tm_x
y = tm_y
dx = tm_dx
dy = tm_dy

# better
x, y, dx, dy = (x + dx * t,
                y + dy * t,
                func(m, x, y, dx, dy),
                func(m, x, y, dx, dy))

# string
# not
s += ',' + name
# better
',.join(names)'

# not
del names[0]
names.pop(0)
names.insert(0, 'mark')

# better
names = deque(names)

del names[0]
names.popleft()
names.appendleft('mark')

# not
def web_lookup(url, saved={}):
    if url in saved:
        return saved[url]
    page = urllib.urlopen(url).read()
    saved[url] = page
    return page

# better
@cache
def web_lookup(url):
    return page = urllib.urlopen(url).read()

# how to open file_name
# not
f = open('dat.txt')
try:
    data = f.read()
finally:
    f.close()

# better
with open('fat.txt') as f:
    data = f.read()

# how to use lock
# make a lock
lock = threading.Lock()
# old way to use a Lock
lock.acquire()
try:
    print('critical1')
finally:
    lock.release()

# new way
with lock:
    print('critical')

# not
try:
    os.remove('somefile')
except OSError:
    pass
# better
with ignored(OSError):
    os.remove('somefile')
@contextmanager
def ignored(*exception):
    try:
        yield
    except exception:
        pass
# better
with open('help.txt', 'w') as f:
    with redirect_stdout(f):
        help(pow)
@contextmanager
def redirect_stdout(fileobj):
    oldstdout = sys.stdout
    sys.stdout = fileobj
    try:
        yield fileobj
    finally:
        sys.stdout = oldstdout
# one logical line of code equals one sentence in English
