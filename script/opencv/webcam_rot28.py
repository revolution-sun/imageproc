
import sys
import cv2
import numpy as np
import sys
import matplotlib.pyplot as plt
import time



def scaling_img():
    print(cv2.__version__)
    path = "/home/sun/Project/raspberry/Jupiter/dataSet/test_img/misc/"

    ## 0 - grey, -1 default
    img_path1 = path + '4.2.01.tiff'
    img_path2 = path + '4.2.04.tif'
    img1 = cv2.imread(img_path1, 1)
    print(img1.shape)
    output = cv2.resize(img1, None, fx=0.5, fy=.5, interpolation=cv2.INTER_LINEAR)
    print(output.shape)
    cv2.imshow('output', output)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
def traslation_img():
    path = "/home/sun/Project/raspberry/Jupiter/dataSet/test_img/misc/"

    ## 0 - grey, -1 default
    img_path1 = path + '4.2.01.tiff'
    img_path2 = path + '4.2.04.tif'
    img1 = cv2.imread(img_path1, 1)
    img2 = cv2.imread(img_path2, 1)

    img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2RGB)

    rows, columns, channels = img1.shape
    ## [[1,0,50].[0,1.30]] meaning ?
    Tran = np.float32([[1, 0, 0],[0, 1, 0]] )
    print(Tran)

    output = cv2.warpAffine(img1, Tran, (columns, rows))
    plt.imshow(output)
    plt.title('shift img')
    plt.show()
    cv2.destroyAllWindows()
def rotation_img():
    path = "/home/sun/Project/raspberry/Jupiter/dataSet/test_img/misc/"

    ## 0 - grey, -1 default
    img_path1 = path + '4.2.01.tiff'
    img1 = cv2.imread(img_path1, 1)


    rows, columns, channels = img1.shape
    angle = 0
    ## [[1,0,50].[0,1.30]] meaning ?
    while True:
        if angle == 360:
            angle = 0
        R = cv2.getRotationMatrix2D((columns, rows), angle, 1)
        print(R)

        output = cv2.warpAffine(img1, R, (columns *2, rows*2))
        cv2.imshow('Rotation', output)
        angle = angle + 1
        time.sleep(0.1)
        if cv2.waitKey(1) ==27:
            break

    cv2.destroyAllWindows()


def rotation_video():

    window_name = 'Living Video'
    cv2.namedWindow(window_name)
    cap = cv2.VideoCapture(0)
    ## 0 - grey, -1 default
    ret, frame = cap.read()

    rows, columns, channels = frame.shape
    angle = 0
    ## [[1,0,50].[0,1.30]] meaning ?
    scale = 0.5
    while True:
        if angle == 360:
            angle = 0
        ret, frame = cap.read()
        if scale < 2:
            scale = scale + 0.1
        else:
            scale = 0.5
        R = cv2.getRotationMatrix2D((columns/2, rows/2), angle, scale)

        print(R)

        output = cv2.warpAffine(frame, R, (columns, rows))
        cv2.imshow(window_name, output)
        angle = angle + 1
        time.sleep(0.5)
        if cv2.waitKey(1) ==27:
            break

    cv2.destroyAllWindows()

if __name__ == '__main__':
    rotation_video()
