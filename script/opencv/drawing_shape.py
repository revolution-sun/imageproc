import cv2
import numpy as np
## drawing geomatric shape
import matplotlib.pyplot as plt
##
window_name = 'Drawing'
img = np.zeros((512, 512, 3), np.uint8)
cv2.namedWindow(window_name)


def draw_circle(event, x, y, flags, param):
    if event == cv2.EVENT_LBUTTONDBLCLK:
        cv2.circle(img, (x, y), 40, (0, 255, 0),-1)
    #print(x,y)
    print(param)

cv2.setMouseCallback(window_name, draw_circle)


def main():

    img = np.zeros((512,512,3), np.uint8)

    cv2.line(img, (0, 180), (99, 0), (255, 0, 0), 2)
    cv2.rectangle(img, (40, 60), (180, 270), (0, 255, 0), 2)
    cv2.circle(img, (60, 60), 30, (0, 0, 255), 5)
    cv2.ellipse(img, (160, 260), (50, 23), 0, 0, 120, (123,123,0), -1)
    points = np.array([[80, 20], [10, 0], [21, 21], [2, 12], [0, 32]],np.int32)
    points = points.reshape((-1, 1, 2))
    cv2.polylines(img, [points], True, (0, 255, 255))
    text = 'Test Text'
    cv2.putText(img, text, (200,200), cv2.FONT_HERSHEY_SIMPLEX, 6, (12,32,255))
    cv2.imshow('Lena', img)
    cv2.waitKey(0)
    cv2.destroyWindow('Lena')

def test():

    while(True):
        cv2.imshow(window_name, img)
        if cv2.waitKey(20) == 27:
            break


if __name__ == '__main__':
    test()
