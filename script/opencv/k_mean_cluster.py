import cv2
import matplotlib.pyplot as plt
import numpy as np

def k_mean_method():
    path = "/home/sun/Project/raspberry/Jupiter/dataSet/"
    img_path = path + '4.1.05.tiff'

    img = cv2.imread(img_path, 1)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    z = img.reshape((-1, 1))
    z = np.float32(z)

    ceriteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
    k = 2
    ret, label1, center1 = cv2.kmeans(z, k, None, ceriteria, 10, cv2.KMEANS_RANDOM_CENTERS)
    center1 = np.uint8(center1)
    res1 = center1[label1.flatten()]
    print(res1)
    output1 = res1.reshape(img.reshape)
    output2 = img
    output3 = img

    output = [output1, output2, output3]

    for i in range(3):
        plt.subplot(1, 3, i+1)
        plt.imshow(output[i])

    plt.show()


if __name__ == '__main__':
    k_mean_method()
