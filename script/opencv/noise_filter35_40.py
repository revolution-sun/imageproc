import numpy as np
import cv2
import matplotlib.pyplot as plt
import time
import random


def noise_():

    path = "/home/sun/Project/raspberry/Jupiter/dataSet/"

    img_path1 = path + '4.2.01.tiff'
    img = cv2.imread(img_path1, 1)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    rows, columns, channels = img.shape

    output = np.zeros(img.shape, np.uint8)
    p = 0.05

    for i in range(rows):
        for j in range(columns):
            r = random.random()
            if r < p / 2:
                # pepper sprinkied
                output[i][j] = [0, 0, 0]
            elif r < p:
                # salt sprinkied
                output[i][j] = [255, 255, 255]
            else:
                output[i][j] = img[i][j]
    plt.imshow(output)
    plt.title('Image with salt and pepper noises')
    plt.show()


def kernel_test():
    path = "/home/sun/Project/raspberry/Jupiter/dataSet/"

    img_path1 = path + '4.2.01.tiff'
    img = cv2.imread(img_path1, 1)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    # rows, columns, channels = img.shape
    ker = np.array(([-1, -1, -1],
                    [-1, 8, -1],
                    [-1, -1, -1]), np.float32)
    output = cv2.filter2D(img, -1, ker)

    plt.subplot(1, 2, 1)
    plt.imshow(img)
    plt.title('Original')
    plt.subplot(1, 2, 2)
    plt.imshow(output)
    plt.title('kernel')
    plt.show()


def low_pass_filter():
    path = "/home/sun/Project/raspberry/Jupiter/dataSet/"

    img_path1 = path + '4.2.07.tiff'
    img = cv2.imread(img_path1, 1)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    # rows, columns, channels = img.shape
    box = cv2.boxFilter(img, -1, (2, 2), normalize=False)
    blur = cv2.blur(img, (2, 2))
    gaussian = cv2.GaussianBlur(img, (117, 117), 0)
    output = [img, box, blur, gaussian]
    title = ['Original', 'Box', 'Blur', 'Gauss']

    for i in range(4):
        plt.subplot(2, 2, i+1)
        plt.imshow(output[i])
        plt.title(title[i])
        plt.xticks([])
        plt.yticks([])
    plt.show()

def media_blur():

    path = "/home/sun/Project/raspberry/Jupiter/dataSet/"

    img_path1 = path + '4.2.01.tiff'
    img = cv2.imread(img_path1, 1)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    rows, columns, channels = img.shape

    noise = np.zeros(img.shape, np.uint8)
    p = 0.05

    for i in range(rows):
        for j in range(columns):
            r = random.random()
            if r < p / 2:
                # pepper sprinkied
                noise[i][j] = [0, 0, 0]
            elif r < p:
                #s alt sprinkied
                noise[i][j] = [255, 255, 255]
            else:
                noise[i][j] = img[i][j]
    denoised = cv2.medianBlur(noise, 5)

    output = [img, noise, denoised]
    title = ['Original', 'Noisy', 'Denoised']

    for i in range(3):
        plt.subplot(1, 3, i+1)
        plt.imshow(output[i])
        plt.title(title[i])
        plt.xticks([])
        plt.yticks([])
    plt.show()


def high_pass_filter():

    path = "/home/sun/Project/raspberry/Jupiter/dataSet/"

    img_path1 = path + '5.1.11.tiff'
    img = cv2.imread(img_path1, 1)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    print(cv2.CV_64F)
    edge = cv2.Laplacian(img, -1, ksize=3, scale=1, delta=0,
                        borderType = cv2.BORDER_DEFAULT)
    # edgesx = cv2.Sobel(img, -1, dx=2, dy=0, ksize=11, scale=1,
    #                    delta=0, borderType=cv2.BORDER_DEFAULT)
    # edgesy = cv2.Sobel(img, -1, dx=0, dy=2, ksize=11, scale=1,
    #                    delta=0, borderType=cv2.BORDER_DEFAULT)
    edgesx = cv2.Scharr(img, -1, dx=1, dy=0, scale=1,
                        delta=0, borderType=cv2.BORDER_DEFAULT)
    edgesy = cv2.Scharr(img, -1, dx=0, dy=1, scale=1,
                        delta=0, borderType=cv2.BORDER_DEFAULT)
    edges = edgesx + edgesy
    output = [img, edge, edges, edgesx, edgesy]
    title = ['Original', 'Laplacian', 'Scharr', 'Scharr-X', 'Scharr-Y']

    for i in range(5):
        plt.subplot(2, 3, i+1)
        plt.imshow(output[i])
        plt.title(title[i])
        plt.xticks([])
        plt.yticks([])
    plt.show()


def canny_detect():

    path = "/home/sun/Project/raspberry/Jupiter/dataSet/"

    img_path1 = path + '4.2.07.tiff'
    img = cv2.imread(img_path1, 0)
    # img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    L1 = cv2.Canny(img, 50, 300, L2gradient=False)
    L2 = cv2.Canny(img, 100, 150, L2gradient=True)
    output = [img, L1, L2]
    title = ['Original', 'L1-norm', 'L2-norm']

    for i in range(3):
        plt.subplot(1, 3, i+1)
        plt.imshow(output[i], cmap='gray')
        plt.title(title[i])
        plt.xticks([])
        plt.yticks([])
    plt.show()


if __name__ == '__main__':
    canny_detect()
