import cv2
import matplotlib.pyplot as plt
import numpy as np


def matplt_histo():

    path = "/home/sun/Project/raspberry/Jupiter/dataSet/"
    img_path = path + '4.2.07.tiff'
    img = cv2.imread(img_path, 0)
    img2 = cv2.imread(img_path, 1)

    img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2RGB)
    R, G, B = cv2.split(img)
    plt.subplot(1,2,1)
    plt.imshow(img, cmap='gray')
    plt.title('Image')
    plt.xticks([])
    plt.yticks([])

    plt.subplot(1, 2, 2)
    plt.hist(img.ravel(), 256, [0,255])
    a = img.ravel()
    print(a.shape)
    plt.title('Histogram')
    plt.xlim([0,255])
    plt.show()

def numpy_histo():

    path = "/home/sun/Project/raspberry/Jupiter/dataSet/"
    img_path = path + '4.2.07.tiff'
    img = cv2.imread(img_path, 0)
    img2 = cv2.imread(img_path, 1)

    img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2RGB)
    R, G, B = cv2.split(img2)
    plt.subplot(1,2,1)
    plt.imshow(img, cmap='gray')
    plt.title('Image')
    plt.xticks([])
    plt.yticks([])

    plt.subplot(1, 2, 2)
    hist, bins = np.histogram(img.ravel(), 256, [0,255])
    #plt.hist(R.ravel(), 256, [0,255])
    plt.plot(hist, color='r')
    plt.title('Histogram')
    plt.xlim([0,255])
    plt.ylim([0,4000])
    plt.show()

    a = np.array([1,2,3,4,5,6,1,1,1,2])
    hista, binsa = np.histogram(a)
    print(hista)

def opencv_histo():

    path = "/home/sun/Project/raspberry/Jupiter/dataSet/"
    img_path = path + '4.2.07.tiff'
    img = cv2.imread(img_path, 0)
    img2 = cv2.imread(img_path, 1)

    img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2RGB)

    red_hist = cv2.calcHist([img2], [0], None, [256], [0,255])
    green_hist = cv2.calcHist([img2], [1], None, [256], [0,255])
    blue_hist = cv2.calcHist([img2], [0], None, [256], [0,255])

    plt.subplot(1,3,1)
    plt.plot(red_hist, color='r')
    plt.title('R')
    plt.xlim([0,255])

    plt.subplot(1,3,2)
    plt.plot(green_hist, color='g')
    plt.title('G')
    plt.xlim([0,255])

    plt.subplot(1,3,3)
    plt.plot(blue_hist, color='b')
    plt.title('B')
    plt.xlim([0,255])

    plt.show()


def oclache_histo():

    path = "/home/sun/Project/raspberry/Jupiter/dataSet/"
    img_path = path + '4.2.04.tif'
    img = cv2.imread(img_path, 1)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    R, G, B = cv2.split(img)

    output_R  = cv2.equalizeHist(R)
    output_G  = cv2.equalizeHist(G)
    output_B  = cv2.equalizeHist(B)

    output_all = cv2.merge((output_R, output_G, output_B))

    #clache1 = cv2.createCLAHE()
    #cl1 = clache1.apply(img)
    #clache2 = cv2.createCLAHE(clipLimit=21.0, tileGridSize=(8,8))
    #cl2 = clache2.apply(img)
    #output1 = cv2.equalizeHist(img)
    #output1 = cl1
    #output2 = cl2
    #output = [img, output1, output2]
    output = [img,output_all, img]
    title = ["original", 'K=2', 'K=4']


    for i in range(3):
        plt.subplot(1,3,i+1)
        plt.imshow(output[i], cmap='gray')
        plt.title(title[i])
        plt.xticks([])
        plt.yticks([])

    plt.show()

if __name__ == '__main__':
    oclache_histo()
