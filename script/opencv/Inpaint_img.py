import numpy as np
import cv2

import matplotlib.pyplot as plt


def inpaint_img():

    path = "/home/sun/Project/raspberry/Jupiter/dataSet/"

    img_path1 = path + '4.2.01.tiff'
    mask_path = path + '4.2.03.tiff'

    img = cv2.imread(img_path1, 1)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    mask = cv2.imread(mask_path, 0)
    #mask = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)


    output1 = cv2.inpaint(img, mask, 5, cv2.INPAINT_TELEA)

    output2 = cv2.inpaint(img, mask, 5, cv2.INPAINT_NS)

    output = [img, mask, output1, output2]

    title = ['Damaged', 'Mask', 'Telea', 'NS']


    for i in range(4):
        plt.subplot(2,2,i+1)

        if i==1:
            plt.imshow(output[i], cmap='gray')
        else:
            plt.imshow(output[i])
        plt.title(title[i])
        plt.xticks([])
        plt.yticks([])
    plt.show()

if __name__ == '__main__':
    inpaint_img()
