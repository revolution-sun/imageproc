import cv2
import numpy as np
import matplotlib.pyplot as plt


def morph_opt():
    print(cv2.__version__)
    path = "/home/sun/Project/raspberry/Jupiter/dataSet/"

    # 0 - grey, -1 default
    # img_path1 = path + 'cameraman.tif'
    img_path1 = path + '4.2.06.tiff'
    img1 = cv2.imread(img_path1, 1)
    img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)

    th = 127
    max_val = 255

    ret, binary_inv = cv2.threshold(img1, th, max_val, cv2.THRESH_BINARY_INV)
    # k = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
    # k = np.ones((5, 5), np.uint8)
    k = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))
    # k = cv2.getStructuringElement(cv2.MORPH_CROSS, (5, 5))
    # print(k)
    erosion = cv2.erode(binary_inv, k, iterations=1)
    dilation = cv2.dilate(binary_inv, k, iterations=1)
    gradient = cv2.morphologyEx(binary_inv, cv2.MORPH_GRADIENT, k)
    output = [img1, binary_inv, erosion, dilation, gradient]
    title = ['original', 'binary', 'erosion', 'dilation', 'gradient']
    print(erosion)
    output_len = len(output)
    for i in range(output_len):
        plt.subplot(1, output_len, i+1)
        plt.imshow(output[i], cmap='gray')
        plt.title(title[i])
        plt.xticks([])
        plt.yticks([])

    plt.show()


def contour_opt():
    print(cv2.__version__)
    path = "/home/sun/Project/raspberry/Jupiter/dataSet/"
    # 0 - grey, -1 default
    # img_path1 = path + 'cameraman.tif'
    img_path1 = path + '4.2.07.tiff'
    img1 = cv2.imread(img_path1, 1)
    img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2RGB)
    gray = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)

    th = 127
    max_val = 255

    ret, thresh = cv2.threshold(gray, th, max_val, 0)
    countour, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE,
                                            cv2.CHAIN_APPROX_SIMPLE)
    print(countour)
    print(hierarchy)

    cv2.drawContours(img1, countour, -1, (0, 0, 255), 2)
    img0 = cv2.imread(img_path1, 1)
    img0 = cv2.cvtColor(img0, cv2.COLOR_BGR2RGB)

    output = [img0, img1]
    title = ['original', 'contour']

    output_len = len(output)
    for i in range(output_len):
        plt.subplot(1, output_len, i+1)
        plt.imshow(output[i])
        plt.title(title[i])
        plt.xticks([])
        plt.yticks([])

    plt.show()


def motion_track():

    w = 800
    h = 600

    cap = cv2.VideoCapture(0)

    cap.set(3, w)
    cap.set(4, h)

    if(cap.isOpened()):
        ret, frame = cap.read()
    else:
        ret = False
    ret, frame1 = cap.read()
    ret, frame2 = cap.read()
    while ret:
        d = cv2.absdiff(frame1, frame2)
        gray = cv2.cvtColor(d, cv2.COLOR_BGR2GRAY)
        blur = cv2.GaussianBlur(gray, (5, 5), 0)

        ret, th = cv2.threshold(blur, 20, 255, cv2.THRESH_BINARY)
        cv2.imshow('Original', frame2)
        cv2.imshow('Intermediate', th)
        dilation = cv2.dilate(th, np.ones((3, 3), np.uint8), iterations=1)
        cv2.imshow('Dilate', dilation)

        if cv2.waitKey(3) == 27:  # exit on ESC
            break
        frame1 = frame2
        ret, frame2 = cap.read()
    cv2.destroyAllWindows()


def max_RGB():

    w = 800
    h = 600

    cap = cv2.VideoCapture(0)

    cap.set(3, w)
    cap.set(4, h)

    if(cap.isOpened()):
        ret, frame = cap.read()
    else:
        ret = False
    ret, frame1 = cap.read()
    ret, frame2 = cap.read()

    while ret:
        d = cv2.absdiff(frame1, frame2)
        gray = cv2.cvtColor(d, cv2.COLOR_BGR2GRAY)
        blur = cv2.GaussianBlur(gray, (5, 5), 0)
        (B, G, R) = cv2.split(frame2)

        M = np.maximum(np.maximum(R, G), B)
        R[R < M] = 0
        G[G < M] = 0
        B[B < M] = 0

        frame_max = cv2.merge((B, R, G))
        ret, th = cv2.threshold(blur, 20, 255, cv2.THRESH_BINARY)
        cv2.imshow('Original', frame2)
        cv2.imshow('Max_RGB', frame_max)
        dilation = cv2.dilate(th, np.ones((3, 3), np.uint8), iterations=1)

        if cv2.waitKey(3) == 27:  # exit on ESC
            break
        frame1 = frame2
        ret, frame2 = cap.read()
    cv2.destroyAllWindows()


if __name__ == '__main__':
    morph_opt()
