import cv2
import numpy as np


def color_track():
    cap = cv2.VideoCapture(0)

    while True:
        ret, frame = cap.read()
        path = "/home/sun/Project/raspberry/Jupiter/dataSet/"

        img_path1 = path + '4.2.06.tiff'
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        lower_blue = np.array([110, 50, 50])
        upper_blue = np.array([130, 255, 255])

        mask = cv2.inRange(hsv, lower_blue, upper_blue)

        res = cv2.bitwise_and(frame, frame, mask)
        print(mask)
        cv2.imshow('frame', frame)

        cv2.imshow('mask', mask)

        cv2.imshow('res', res)

        if cv2.waitKey(1) & 0xFF == 27:
            break
    cv2.destroyAllWindows()


def find_hsv():

    green = np.uint8([[[0, 255, 0]]])

    hsv_green = cv2.cvtColor(green, cv2.COLOR_BGR2HSV)

    print(hsv_green)


if __name__ == '__main__':
    find_hsv()
