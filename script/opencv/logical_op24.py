
import sys
import cv2
import numpy as np
import sys
import matplotlib.pyplot as plt
import time


def bitwise_operating():
    print(cv2.__version__)
    path = "/home/sun/Project/raspberry/Jupiter/dataSet/"

    # 0 - grey, -1 default
    img_path1 = path + '4.2.01.tiff'
    img_path2 = path + '4.2.04.tif'
    img1 = cv2.imread(img_path1, 1)
    img2 = cv2.imread(img_path2, 1)

    img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2RGB)

    img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2RGB)
    img3 = cv2.bitwise_not(img1)

    img4 = cv2.bitwise_and(img1, img2)
    img5 = cv2.bitwise_or(img1, img2)
    img6 = cv2.bitwise_xor(img1, img2)

    titles = ['Image1', 'Image2', 'Not', 'And', 'OR', 'XOR']
    images = [img1, img2, img3, img4, img5, img6]
    img_len = len(images)
    ## probulem why is not gray
    for i in range(img_len):
        plt.subplot(2,3, i+1)
        plt.imshow(images[i])
        plt.title(titles[i])
        plt.xticks([])
        plt.yticks([])

    plt.show()
    cv2.destroyAllWindows()
def test():
	print(cv2.__version__)


if __name__ == '__main__':
    bitwise_operating()
