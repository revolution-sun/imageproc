
import sys
import cv2
import numpy as np
import sys
import matplotlib.pyplot as plt
import time



def scaling_img():
    print(cv2.__version__)
    path = "/home/sun/Project/raspberry/Jupiter/dataSet/"

    ## 0 - grey, -1 default
    img_path1 = path + '4.2.01.tiff'
    img_path2 = path + '4.2.04.tif'
    img1 = cv2.imread(img_path1, 1)
    print(img1.shape)
    output = cv2.resize(img1, None, fx=0.5, fy=.5, interpolation=cv2.INTER_LINEAR)
    print(output.shape)
    cv2.imshow('output', output)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def traslation_img():
    path = "/home/sun/Project/raspberry/Jupiter/dataSet/"

    ## 0 - grey, -1 default
    img_path1 = path + '4.2.01.tiff'
    img_path2 = path + '4.2.04.tif'
    img1 = cv2.imread(img_path1, 1)
    img2 = cv2.imread(img_path2, 1)

    img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2RGB)

    rows, columns, channels = img1.shape
    # [[1,0,50].[0,1.30]] meaning ?
    Tran = np.float32([[1, 0, 30], [0, 1, 10]])
    # print(Tran)

    output = cv2.warpAffine(img1, Tran, (columns, rows))
    print(output)
    plt.imshow(output)
    plt.title('shift img')
    plt.show()
    cv2.destroyAllWindows()


def rotation_img():
    path = "/home/sun/Project/raspberry/Jupiter/dataSet/"

    ## 0 - grey, -1 default
    img_path1 = path + '4.2.01.tiff'
    img_path2 = path + '4.2.04.tif'
    img1 = cv2.imread(img_path1, 1)
    img2 = cv2.imread(img_path2, 1)

    img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2RGB)

    rows, columns, channels = img1.shape
    # [[1,0,50].[0,1.30]] meaning ?
    R = cv2.getRotationMatrix2D((columns/2, rows/2), 0, 0.5)

    print(R)

    output = cv2.warpAffine(img1, R, (columns, rows))
    plt.imshow(output)
    plt.title('shift img')
    plt.show()
    cv2.destroyAllWindows()
def affine_img():
    path = "/home/sun/Project/raspberry/Jupiter/dataSet/"

    img_path1 = path + '4.2.01.tiff'
    img1 = cv2.imread(img_path1, 1)
    rows, columns, channels = img1.shape
    angle = 0
    while True:
        if angle == 360:
            angle = 0
        point1 = np.float32([[100, 100], [300, 100], [100, 300]])
        point2 = np.float32([[200, 150], [400, 150], [100, 400]])
        A = cv2.getAffineTransform(point1, point2)
        print(A)
        output = cv2.warpAffine(img1, A, (columns, rows))
        cv2.imshow('Rotation', output)
        angle = angle + 1
        time.sleep(0.1)
        if cv2.waitKey(1) == 27:
            break

    cv2.destroyAllWindows()


def perspective_img():
    path = "/home/sun/Project/raspberry/Jupiter/dataSet/"

    img_path1 = path + '4.2.01.tiff'
    img1 = cv2.imread(img_path1, 1)

    rows, columns, channels = img1.shape
    angle = 0
    while True:
        if angle == 360:
            angle = 0
        point1 = np.float32([[0, 0],[300,0],[0,300],[300,200]])
        point2 = np.float32([[0, 0],[400,0],[0,400],[400,400]])

        P = cv2.getPerspectiveTransform(point2, point1)
        print(P)

        output = cv2.warpPerspective(img1, P, (500, 400))
        cv2.imshow('Perspective', output)
        angle = angle + 1
        time.sleep(0.1)
        if cv2.waitKey(1) == 27:
            break

    cv2.destroyAllWindows()

if __name__ == '__main__':
    traslation_img()
