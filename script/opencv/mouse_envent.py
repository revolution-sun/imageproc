# importing

import cv2


def main():
    envents = [i for i in dir(cv2) if 'EVENT' in i]
    print(envents)


if __name__ == '__main__':
    main()
