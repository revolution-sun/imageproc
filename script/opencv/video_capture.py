import cv2
import numpy as np
import matplotlib.pyplot as plt

def video_capture_test():
    window_name = 'Live Video Feed'
    cv2.namedWindow(window_name)
    cap = cv2.VideoCapture(0)
    i = 0
    print('width:' + str(cap.get(3)))
    print('height:' + str(cap.get(4)))
    # record
    file_name = "/home/sun/Project/raspberry/Jupiter/dataSet/test_img/output.avi"
    codec = cv2.VideoWriter_fourcc('X','V','I','D')
    frame_rate = 30
    resolution = (640, 480)
    VideoFileOutput = cv2.VideoWriter(file_name, codec, frame_rate, resolution)
    #cap.set(3, 1024)
    #cap.set(4, 768)

    while(True):
        ret, frame = cap.read()
        gray = cv2.cvtColor(frame, 0)

        cv2.imshow(window_name, gray)
        VideoFileOutput.write(frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        wid = cap.get(3)
        dep = cap.get(4)
        #print(wid,dep)
    cap.release()
    VideoFileOutput.release()
    cv2.destroyAllWindows()

def play_video():

    window_name = 'Live Video play'
    cv2.namedWindow(window_name)
    # record
    file_name = "/home/sun/Project/raspberry/Jupiter/dataSet/test_img/output.avi"
    cap = cv2.VideoCapture(file_name)

    while(cap.isOpened()):
        ret, frame = cap.read()

        print(ret)

        if(ret):
            cv2.imshow(window_name, frame)
            if cv2.waitKey(33) & 0xFF == ord('q'):
                break
        else:
            break
    cap.release()
    cv2.destroyAllWindows()

def object_tracking():
    window_name = 'Preview'
    #cv2.namedWindow(window_name)
    cap = cv2.VideoCapture(0)

    file_name = "/home/sun/Project/raspberry/Jupiter/dataSet/test_img/output.avi"
    codec = cv2.VideoWriter_fourcc('X','V','I','D')
    frame_rate = 30
    resolution = (640, 480)

    VideoFileOutput = cv2.VideoWriter(file_name, codec, frame_rate, resolution)
    #cap.set(3, 1024)
    #cap.set(4, 768)
    if cap.isOpened():
        ret, frame = cap.read()
    else:
        ret = False
    while ret:
        ret, frame = cap.read()
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

        # blue color
        #low = np.array([100, 50, 50])
        #high = np.array([140, 255, 255])
        #green color
        low = np.array([20, 70, 70])
        high = np.array([80, 255, 255])


        img_mask = cv2.inRange(hsv, low, high)
        output = cv2.bitwise_and(frame, frame, mask = img_mask)
        print(img_mask)

        cv2.imshow('Original', frame)
        cv2.imshow('Image mask',img_mask)
        cv2.imshow('Original webcam Feed', hsv)
        cv2.imshow('color tracking', output)

        VideoFileOutput.write(frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        #print(wid,dep)
        #print(i)
    cap.release()
    VideoFileOutput.release()
    cv2.destroyAllWindows()


def cap_test():

    cap = cv2.VideoCapture(0)

    if cap.isOpened():
        ret, frame = cap.read()
        print(ret)
        print(frame)
    else:
        ret = False

    img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

    plt.imshow(img)
    plt.title('color image RGB')
    plt.show()
if __name__ == '__main__':
    #capture_test()
    cap_test()
