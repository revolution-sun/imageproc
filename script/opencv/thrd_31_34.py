
import sys
import cv2
import numpy as np
import matplotlib.pyplot as plt


def Thresh_img():

    e1 = cv2.getTickCount()
    path = "/home/sun/Project/raspberry/Jupiter/dataSet/"
    img_path1 = path + '7.1.07.tiff'
    img1 = cv2.imread(img_path1, 1)
    img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2RGB)

    th = 127
    max_val = 255
    ret, o1 = cv2.threshold(img1, th, max_val, cv2.THRESH_BINARY)
    ret, o2 = cv2.threshold(img1, th, max_val, cv2.THRESH_BINARY_INV)
    ret, o3 = cv2.threshold(img1, th, max_val, cv2.THRESH_TOZERO)
    ret, o4 = cv2.threshold(img1, th, max_val, cv2.THRESH_TOZERO_INV)
    ret, o5 = cv2.threshold(img1, th, max_val, cv2.THRESH_TRUNC)
    output = [img1, o1, o2, o3, o4, o5]
    title = ['original', 'BIN', 'BIN_INV', 'Zeros', 'Zero INV', 'Truc']
    for i in range(6):
        plt.subplot(2,3, i+1)
        plt.imshow(output[i])
        plt.title(title[i])
        plt.xticks([])
        plt.yticks([])

    e2 = cv2.getTickCount()
    time = (e2 - e1) / cv2.getTickFrequency()
    print(time)
    plt.show

def otsu_thr_img():
    print(cv2.__version__)
    path = "/home/sun/Project/raspberry/Jupiter/dataSet/test_img/misc/"

    ## 0 - grey, -1 default
    img_path1 = path + '7.1.07.tiff'
    img1 = cv2.imread(img_path1, 0)
    #img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2RGB)

    #plt.imshow(img1)
    th = 0
    max_val = 255
    ret, o1 = cv2.threshold(img1, th, max_val, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    ret, o2 = cv2.threshold(img1, th, max_val, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
    ret, o3 = cv2.threshold(img1, th, max_val, cv2.THRESH_TOZERO + cv2.THRESH_OTSU)
    ret, o4 = cv2.threshold(img1, th, max_val, cv2.THRESH_TOZERO_INV + cv2.THRESH_OTSU)
    ret, o5 = cv2.threshold(img1, th, max_val, cv2.THRESH_TRUNC + cv2.THRESH_OTSU)
    output = [img1, o1, o2, o3, o4, o5]

    title = ['original', 'BIN', 'BIN_INV', 'Zeros', 'Zero INV', 'Truc']
    for i in range(6):
        plt.subplot(2,3, i+1)
        plt.imshow(output[i], cmap='gray')
        plt.title(title[i])
        plt.xticks([])
        plt.yticks([])

    plt.show()

def adpt_thr_img():
    print(cv2.__version__)
    path = "/home/sun/Project/raspberry/Jupiter/dataSet/test_img/misc/"

    ## 0 - grey, -1 default
    img_path1 = path + '5.1.12.tiff'
    img1 = cv2.imread(img_path1, 0)
    #img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2RGB)

    #plt.imshow(img1)
    block_size  = 223
    constant = 2

    thr1 = cv2.adaptiveThreshold(img1, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, block_size, constant)
    thr2 = cv2.adaptiveThreshold(img1, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, block_size, constant)
    output = [img1, thr1, thr2]

    title = ['original', 'mean', 'gaussian']
    for i in range(3):
        plt.subplot(1,3, i+1)
        plt.imshow(output[i], cmap='gray')
        plt.title(title[i])
        plt.xticks([])
        plt.yticks([])

    plt.show()

def thr_living_video():
    window_name = 'Preview'
    #cv2.namedWindow(window_name)
    cap = cv2.VideoCapture(0)

    ret = True
    while ret:
        ret, frame = cap.read()
        th = 127
        max_val = 255
        algo = cv2.THRESH_BINARY
        ret, o1 = cv2.threshold(frame, th, max_val, algo)
        ret, o2 = cv2.threshold(frame, th, max_val, cv2.THRESH_BINARY_INV)
        ret, o3 = cv2.threshold(frame, th, max_val, cv2.THRESH_TOZERO)
        ret, o4 = cv2.threshold(frame, th, max_val, cv2.THRESH_TOZERO_INV)
        ret, o5 = cv2.threshold(frame, th, max_val, cv2.THRESH_TRUNC)

        cv2.imshow(window_name + '0', o1)
        cv2.imshow(window_name  + '1', o2)
        cv2.imshow(window_name + '2', o3)
        cv2.imshow(window_name + '3', o4)
        cv2.imshow(window_name + '4', o5)

        if cv2.waitKey(1) == 27:
            break
    cv2.destroyAllWindows()
    cap.release()




if __name__ == '__main__':
    Thresh_img()
