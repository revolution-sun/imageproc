import cv2
import numpy as np
import matplotlib.pyplot as plt
import time


def emptyFunc():
    pass


def read_img():
    print(cv2.__version__)
    path = "/home/sun/Project/raspberry/Jupiter/dataSet/"
    img_path = path + 'lena_color_512.tif'
    img = cv2.imread(img_path, 0)
    # img[1:200, 1:200] = 255
    cv2.imshow('Lena', img)
    print(img)
    # cv2.imwrite(out_path, img)
    print(img.dtype)
    print(img.shape)
    print(img.ndim)
    print(img.size)
    cv2.waitKey(0)
    cv2.destroyWindow('Lena')


def write_img():
    path = "/home/sun/Project/raspberry/Jupiter/dataSet/"
    img_path = path + '4.1.05.tiff'
    output = path + '4.1.jpg'
    img = cv2.imread(img_path, 1)
    cv2.imshow('house', img)
    cv2.imwrite(output, img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def extend_bord():
    print(cv2.__version__)
    path = "/home/sun/Project/raspberry/Jupiter/dataSet/"
    img_path = path + 'lena_color_512.tif'
    img = cv2.imread(img_path, 1)
    img2 = cv2.imread(img_path, 0)
    # cv2.imshow('Lena', img)
    BLUE = [255, 0, 0]
    replicate = cv2.copyMakeBorder(img, 10, 10, 10, 12, cv2.BORDER_CONSTANT, value=BLUE)
    # cv2.imshow('replicate', replicate)
    # print(img.dtype)
    # print(img.shape)
    # print(img.ndim)
    # print(img.size)
    print('img2')
    print(img2.shape)
    img3 = img2[120:130, 120:130]
    ret, mask = cv2.threshold(img3, 175, 255, cv2.THRESH_BINARY)

    not_img3 = cv2.bitwise_not(img3)

    print('img3')
    print(img3)
    print('mask')
    print(mask)
    #print(not_img3)
    #cv2.imshow('a', img3)
    cv2.imshow('mask',mask)
    img4_and = cv2.bitwise_and(img3, img3, mask=mask)
    print(img4_and)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def math_operating_img():
    print(cv2.__version__)
    path = "/home/sun/Project/raspberry/Jupiter/dataSet/"

    # 0 - grey, -1 default
    img_path1 = path + 'lena_color_512.tif'
    img_path2 = path + 'lake.tif'
    img1 = cv2.imread(img_path1, 1)
    img2 = cv2.imread(img_path2)
    img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2RGB)
    img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2RGB)
    # arithmetic operations
    add = img1 + 50
    sub = img1 - 50
    sub2 = img2 - img1
    div = img1 / img2
    mult = img1 * 3
    titles = ['lena', 'lake', 'mult', 'div']
    images = [img1, img2, mult, div]
    print(img1.shape)

    img_len = len(images)
    for i in range(img_len):
        plt.subplot(1, img_len, i+1)
        plt.imshow(images[i])
        plt.title(titles[i])
        plt.xticks([])
        plt.yticks([])

    plt.show()

def negative_img():
    print(cv2.__version__)
    path = "/home/sun/Project/raspberry/Jupiter/dataSet/"

    ## 0 - grey, -1 default
    img_path1 = path + '4.2.06.tiff'
    img_path2 = path + 'lake.tif'
    img = cv2.imread(img_path1)
    img1 = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    img2 = cv2.cvtColor(img1, cv2.COLOR_RGB2GRAY)
    img3 = abs(255 - img1)
    img4 = abs(255 - img2)
    #arithmetic operations
    titles = ['Color Image', 'gray-scale', 'color-neg', 'gray-scale-neg']
    images = [img1, img2, img3, img4]
    img_len = len(images)
    #
    for i in range(img_len):
        plt.subplot(1, img_len, i+1)
        plt.imshow(images[i], cmap='gray')
        plt.title(titles[i])
        plt.xticks([])
        plt.yticks([])

    plt.show()
    cv2.destroyAllWindows()


def operating_imgs():
    print(cv2.__version__)
    path = "/home/sun/Project/raspberry/Jupiter/dataSet/"

    # 0 - grey, -1 default
    img_path1 = path + 'lena_color_512.tif'
    img_path2 = path + 'lake.tif'
    img1 = cv2.imread(img_path1, 1)
    img2 = cv2.imread(img_path2, 1)

    add = img1 + img2
    sub = img1 - 50
    sub2 = img2 - img1
    div = img1 / img2
    mult = img1 * 3
    titles = ['lena', 'lake', 'output', 'add']

    alpha = 0
    beta = 1
    gamma = 0
    output = cv2.addWeighted(img1, alpha, img1, beta, gamma)
    images = [img1, img2, output, add]
    for i in np.linspace(0, 1, 50):
        alpha = i
        beta = 1 - i
        print(alpha)
        output = cv2.addWeighted(img1, alpha, img2, beta, gamma)
        cv2.imshow('Transition', output)
        time.sleep(0.25)
        if cv2.waitKey(1) == 27:
            break
    cv2.destroyAllWindows()


def two_img_addition():
    print(cv2.__version__)
    path = "/home/sun/Project/raspberry/Jupiter/dataSet/"

    # 0 - grey, -1 default
    img_path1 = path + 'lena_color_512.tif'
    img_path2 = path + 'lake.tif'
    img1 = cv2.imread(img_path1, 1)
    img2 = cv2.imread(img_path2)
    # img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2RGB)
    # mg2 = cv2.cvtColor(img2, cv2.COLOR_BGR2RGB)
    # arithmetic operations

    alpha = 0.5
    beta = 0.5
    gamma = 0
    output = cv2.addWeighted(img1, alpha, img2, beta, gamma)

    name_window = 'Transition Demo'
    cv2.namedWindow(name_window)
    max_num = 1000
    cv2.createTrackbar('alpha', name_window, 0, max_num, emptyFunc)

    while True:
        cv2.imshow(name_window, output)

        output = cv2.addWeighted(img1, alpha, img2, beta, gamma)
        if cv2.waitKey(12) == 27:
            break
        alpha = cv2.getTrackbarPos('alpha', name_window) / max_num
        beta = 1 - alpha
        output = cv2.addWeighted(img1, alpha, img2, beta, gamma)
        print(alpha, beta)

    cv2.destroyAllWindows()


def test():
    print(cv2.__version__)


if __name__ == '__main__':
    read_img()
