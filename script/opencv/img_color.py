
import sys
import cv2
import numpy as np
import sys
import matplotlib.pyplot as plt
import time

def emptyFunc():
    pass


def main():
    print(cv2.__version__)
    path = "/home/sun/Project/raspberry/Jupiter/dataSet/"

    ## 0 - grey, -1 default
    img_path1 = path + '4.2.01.tiff'
    img_path2 = path + 'lake.tif'
    img = cv2.imread(img_path1)
    img1 = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    r, g, b = cv2.split(img1)
    #arithmetic operations
    titles = ['original-img', 'r', 'g', 'b']
    images = [cv2.merge((r, g, b)), r, g, b]
    cmaps = ['Accent', 'Reds','Greens', 'Blues']
    img_len = len(images)
    ## probulem why is not gray
    for i in range(img_len):
        plt.subplot(1,img_len, i+1)
        plt.imshow(images[i], cmap=cmaps[i])
        plt.title(titles[i])
        plt.xticks([])
        plt.yticks([])

    plt.show()
    cv2.destroyAllWindows()

def track_bar():

    img = np.zeros((512,512,3), np.uint8)
    window_name = "OpenCV BGR Color"
    cv2.namedWindow(window_name)
    cv2.createTrackbar('B', window_name, 0, 255, emptyFunc)
    cv2.createTrackbar('G', window_name, 0, 255, emptyFunc)
    cv2.createTrackbar('R', window_name, 0, 255, emptyFunc)
    while(True):

        cv2.imshow(window_name, img)
        if cv2.waitKey(1) == 27 & 0xFF:
            break
        blue = cv2.getTrackbarPos('B', window_name)
        green = cv2.getTrackbarPos('G', window_name)
        red = cv2.getTrackbarPos('R', window_name)
        img[:] = [blue, green, red]
        print(blue, green, red)
    cv2.destroyWindow(window_name)


def test():
	print(cv2.__version__)


if __name__ == '__main__':
    track_bar()
