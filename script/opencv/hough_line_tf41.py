import cv2
import numpy as np
import matplotlib.pyplot as plt

def hought_line_tf():
    window_name = 'Live Video Feed'
    cv2.namedWindow(window_name)
    cap = cv2.VideoCapture(0)
    if cap.isOpened():
        ret, frame = cap.read()
    else:
        ret = False
    #cap.set(3, 1024)
    #cap.set(4, 768)

    while ret:

        ret, frame = cap.read()
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        edges = cv2.Canny(gray, 50, 250, apertureSize=5, L2gradient=True)

        lines = cv2.HoughLines(edges, 1, np.pi/180, 250)

        if lines is not None:
            for roh, theta in lines[0]:
                a = np.cos(theta)
                b = np.sin(theta)
                x0 = a * roh
                y0 = b * roh
                pts1 = ( int(x0 + 1000*(-b)),  int(y0 + 1000*(a)))
                pts2 = ( int(x0 - 1000*(-b)),  int(y0 - 1000*(a)))
                cv2.line(frame, pts1, pts2, (0, 255, 0), 3)
                print(pts1)
        cv2.imshow(window_name, frame)


        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        #print(wid,dep)
    cap.release()
    cv2.destroyAllWindows()


def cap_test():

    cap = cv2.VideoCapture(0)

    if cap.isOpened():
        ret, frame = cap.read()
        print(ret)
        print(frame)
    else:
        ret = False

    img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

    plt.imshow(img)
    plt.title('color image RGB')
    plt.show()
if __name__ == '__main__':
    #capture_test()
    hought_line_tf()
