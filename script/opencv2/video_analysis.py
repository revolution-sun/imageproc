import cv2
import numpy as np
import matplotlib.pyplot as plt

def video_cap():

    path = "/home/sun/Project/raspberry/Jupiter/dataSet/"
    img_path = path + '4.2.03.tiff'
    ## 0 - grey, -1 default
    img = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)

    #IMREAD_COLOR = 1
    #IMREAD_UNCHANGED = -1
    cv2.imshow('Example', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


if __name__ == '__main__':
    video_cap()
