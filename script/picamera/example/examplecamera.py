import picamera
import time


#camera = picamera.PiCamera()
with picamera.PiCamera() as camera:
	#camera.vflip = True
	camera.resolution = (1280, 720)
	#camera.capture('example.jpg')
	camera.start_recording('examplevid.h264')
	time.sleep(5)
	camera.stop_recording

