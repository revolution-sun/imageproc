#!/usr/bin/expect 
set timeout 3 
set password "pi"
set ip 192.168.21.218
spawn ssh pi@${ip}
expect "*password:*"
send "${password}\r"
expect "*try again*"
exit
interact